#include<stdio.h>
#include<math.h>
float input()
{
    float a;
    printf("enter the value\n");
    scanf("%f",&a);
    return a;
}
float compute(float x1,float y1,float x2,float y2)
{
    float distance;
    distance=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
    return distance;
}
void output(float distance)
{
    printf("the distance between two points is %f",distance);
}
int main()
{
    float x1, x2, y1, y2, distance;
    x1=input();
    y1=input();
    x2=input();
    y2=input();
    distance=compute(x1,y1,x2,y2);
    output(distance);
    return 0;
}