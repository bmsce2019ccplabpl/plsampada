#include <stdio.h>
#include <math.h>

struct Point 
{
    float x;
    float y;
};


 float Distance(struct Point a, struct Point b)
{
    float distance;
    distance = sqrt(pow(b.x-a.x,2)+pow(b.y-a.y,2));
    return distance;
}
int main()
{
    struct Point a, b;
    printf("Enter coordinate of point a: ");
    scanf("%d%d", &a.x, &a.y);
    printf("Enter coordinate of point b: ");
    scanf("%d%d", &b.x, &b.y);
    printf("Distance between a and b is %f\n",Distance(a, b));
    return 0;
}