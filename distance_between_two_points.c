#include<stdio.h>
#include<math.h>

float input()
{
	float x1;
	printf("enter the value of coordinate of a point\n");
	scanf("%f",&x1);
	return x1;
}
float compute(float x1,float x2,float y1, float y2)
{
	float distance=sqrt(pow(x1-x2,2)+pow(y1-y2,2));
	return distance;
}
void output(float result)
{
	printf("the distance between two points a and b is %f\n",result);
}
int main()
{
	float x1=input();
	float y1=input();
	float x2=input();
	float y2=input();
	float distance=compute(x1,x2,y1,y2);
	output(distance);
	return 0;
}