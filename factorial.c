#include<stdio.h>
int input()
{
   int n;
   printf("Enter any number\n");
   scanf("%d",&n);
   return n;
} 

int compute(int a)
{
   int f=1;
   for(int i=1;i<=a;i++)
   {
       f=f*i;
   }
   return f;
}

int output(int a, int b)
{
    printf("The Factorial of the number %d is %d\n",a,b);
    return 0;
}

int main()
{
   int a=input();
   int factorial=compute(a);
   output(a,factorial);
   return 0;
}
